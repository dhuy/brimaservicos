(function() {
  'use strict';

  angular
    .module('brimaservicos.filters')
    .filter('FixComission', FixComission);

  function FixComission() {
    return function(comission) {
      return (!comission) ? 0 : Math.round(comission * 100) / 100;
    };
  }

})();
