(function() {
  'use strict';

  angular
    .module('brimaservicos.filters')
    .filter('PhoneFormat', PhoneFormat);

  function PhoneFormat() {
    return function(input) {
      var str = input + '';

      if (/[a-z]/i.test(str))
        return str;

      str = str.replace(/\D/g,'');

      if (str.length === 11 ) {
        str=str.replace(/^(\d{2})(\d{5})(\d{4})/,'($1) $2-$3');
      } else {
        str=str.replace(/^(\d{2})(\d{4})(\d{4})/,'($1) $2-$3');
      }

      return str;
    };
  }

})();
