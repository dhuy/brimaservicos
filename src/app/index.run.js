(function() {
  'use strict';

  angular
    .module('brimaservicos')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, $state, Toastr, Message, LoginService) {
    /*
     * Function to control token session in private routes
     */
    $rootScope.$on("$stateChangeStart",
      function (event, toState, toParams, fromState, fromParams) {
        if (toState.permission === 'private') {
          if (!$rootScope.user) {
            LoginService.getUserInfo()
              .then(function(response) {
                $rootScope.user = response.data._embedded.user_information[0];
              })
              .catch(function(response, status, headers, config) {
                $state.go('login');

                Toastr.warning(Message.get('session_expired'), null, {timeOut: 3000, progressBar: true});
              })
            ;
          }
        }
      })
    ;

    /*
    * Function to avoid calling jQuery in Controllers
     */
    $rootScope.getElement = function(identifier) {
      return $(identifier);
    };

    /*
     * Function to bootstrap AdminLTE options
     */
    $rootScope.setAdminLTEOptions = function() {
      //Easy access to options
      var o = $.AdminLTE.options;

      //Activate the layout maker
      $.AdminLTE.layout.activate();

      //Enable sidebar tree view controls
      $.AdminLTE.tree('.sidebar');

      //Enable control sidebar
      if (o.enableControlSidebar) {
        $.AdminLTE.controlSidebar.activate();
      }

      //Add slimscroll to navbar dropdown
      if (o.navbarMenuSlimscroll && typeof $.fn.slimscroll != 'undefined') {
        angular.element(".navbar .menu").slimscroll({
          height: o.navbarMenuHeight,
          alwaysVisible: false,
          size: o.navbarMenuSlimscrollWidth
        }).css("width", "100%");
      }

      //Activate fast click
      if (o.enableFastclick && typeof FastClick != 'undefined') {
        FastClick.attach($document.body);
      }
    };
  }

})();
