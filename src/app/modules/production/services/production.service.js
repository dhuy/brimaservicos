(function() {
  'use strict';

  angular
    .module('brimaservicos.production')
    .factory('ProductionService', ProductionService);

  /** @ngInject */
  function ProductionService($http, ENV, LoginService) {
    var factory = {
      getMonthFilter: getMonthFilter,
      getServiceFilter: getServiceFilter,
      getProductionFromFilters: getProductionFromFilters,
      getProductionSummary: getProductionSummary
    };
    return factory;

    function getMonthFilter() {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/filtro_mes',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }

    function getServiceFilter() {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/servicos',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }

    function getProductionFromFilters(urlParams) {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/producao?' + urlParams,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }

    function getProductionSummary(urlParams) {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/producao_resumo?' + urlParams,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }
  }
})();
