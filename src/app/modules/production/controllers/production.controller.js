(function() {
  'use strict';

  angular
    .module('brimaservicos.production')
    .controller('ProductionController', ProductionController);

  /** @ngInject */
  function ProductionController(Spin, Toastr, Message, $filter, $rootScope, ngTableParams, ProductionService) {
    var vm = this,
        urlSummaryParams = null;

    vm.breadcrumbs = [];
    vm.productions = [];
    vm.filters = [];

    vm.addFilter = addFilter;
    vm.removeFilter = removeFilter;
    vm.removeAllFilters = removeAllFilters;
    vm.searchProduction = searchProduction;
    vm.getExternalFilters = getExternalFilters;
    vm.getProductionSummary = getProductionSummary;

    _init();

    function _init() {
      var elToSpin = $rootScope.getElement('body');
      Spin.start(elToSpin, { relative: true });

      vm.breadcrumbs.push({
        name: 'Produção',
        icon: 'fa fa-database',
        state: 'main.production',
        isActive: true
      });

      _runNgTable();

      Spin.stop(elToSpin);
    }

    function addFilter() {
      vm.filters.push({});
    }

    function removeFilter(i) {
      vm.filters.splice(i, 1);
    }

    function removeAllFilters() {
      vm.filters = [];
    }

    function searchProduction() {
      if (!vm.hasFilter()) {
        Toastr.warning(Message.get('choose_at_least_one_filter'), null, {timeOut: 2000, progressBar: true});
        return;
      }

      var urlFilterParams = vm.getUrlFilterParams();

      if (urlFilterParams === '') {
        Toastr.warning(Message.get('choose_at_least_one_filter_type'), null, {timeOut: 2000, progressBar: true});
        return;
      }

      urlSummaryParams = urlFilterParams;
      vm.closeModal();

      var elToSpin = $rootScope.getElement('.production-wrapper .row');
      Spin.start(elToSpin, { relative: true });

      ProductionService.getProductionFromFilters(urlFilterParams)
        .then(function(response) {
          vm.productions = response.data._embedded.producao;

          _updateNgTable();
          vm.insertSummaryButton();

          Spin.stop(elToSpin);
        })
        .catch(function(response, status, headers, config) {
          Spin.stop(elToSpin);

          vm.productions = [];
          _updateNgTable();

          Toastr.warning(Message.get('no_result'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function getExternalFilters() {
      _getMonthFilter();
    }

    function getProductionSummary() {
      var elToSpin = $rootScope.getElement('.production-summary-modal');

      ProductionService.getProductionSummary(urlSummaryParams)
        .then(function(response) {
          vm.productionSummary = _prepareProdSummaryObj(response.data.detail);

          Spin.stop(elToSpin);
        })
        .catch(function(response, status, headers, config) {
          Spin.stop(elToSpin);

          Toastr.error(Message.get('cant_get_filters'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    String.prototype.insertAt = function(index, string) {
      return this.substr(0, index) + string + this.substr(index);
    };

    function _prepareProdSummaryObj(object) {
      var linesObj = [],
          accountsQuantity = {},
          totalObj = {};


      for (var key in object) {
        if (!object.hasOwnProperty(key)) continue;

        if ((key != 'contas') && (key != 'total')) {
          var lineValue = object[key].valor.toString();

          linesObj.push({
            line: key.replace('linha', ''),
            name: object[key].nome,
            value: lineValue.insertAt((lineValue.length - 2), '.'),
            quantity: object[key].qtd
          });
        } else if (key == 'contas') {
          accountsQuantity['name'] = object[key].nome;
          accountsQuantity['quantity'] = object[key].qtd;
        } else if (key == 'total') {
          var totalValue = object[key].valor.toString();

          totalObj['value'] = totalValue.insertAt((totalValue.length - 2), '.');
          totalObj['quantity'] = object[key].qtd;
        }
      }

      return {
        lines: linesObj,
        accounts: accountsQuantity,
        total: totalObj
      }
    }

    function _getMonthFilter() {
      ProductionService.getMonthFilter()
        .then(function(response) {
          vm.monthFilter = response.data.detail;

          _getServiceFilter();
        })
        .catch(function(response, status, headers, config) {
          Toastr.error(Message.get('cant_get_filters'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function _getServiceFilter() {
      ProductionService.getServiceFilter()
        .then(function(response) {
          vm.serviceFilter = response.data.detail;

          vm.popoverSpin = false;
        })
        .catch(function(response, status, headers, config) {
          vm.popoverSpin = false;

          Toastr.error(Message.get('cant_get_filters'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    /**
     * ngTable
     */
    function _updateNgTable() {
      vm.tableParams.reload();
    }

    function _runNgTable() {
      vm.tableParams = new ngTableParams({
        page: 1,
        count: 5
      }, {
        counts: [],
        total: vm.productions.length,
        getData: function ($defer, params) {
          var processedData = vm.productions;

          if (Object.keys(params.sorting()).length !== 0)
            processedData = $filter('orderBy')(processedData, params.orderBy());

          $defer.resolve(processedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          params.total(processedData.length);
          (!processedData.length) ? vm.noResult = true : vm.noResult = false;
        }
      });
    }
  }
})();
