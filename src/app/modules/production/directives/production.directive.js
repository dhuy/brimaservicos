(function() {
  'use strict';

  angular
    .module('brimaservicos.production')
    .directive('production', Production);

  /** @ngInject */
  function Production($timeout, $compile, Spin) {
    var directive = {
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attrs) {
      var vm = scope.vm;

      vm.closeModal = closeModal;
      vm.bindInputMasks = bindInputMasks;
      vm.getUrlFilterParams = getUrlFilterParams;
      vm.hasFilter = hasFilter;
      vm.insertSummaryButton = insertSummaryButton;
      vm.showSummaryModal = showSummaryModal;

      _init();

      function _init() {
        $(document).on('change', $('.filter-select'), function(e) {
          if (!$(e.target).hasClass('filter-select')) return;

          var $el = $(e.target),
            value = $el.val(),
            li = $el.parent(),
            select = li.find('.' + value);

          $.each(li.children('.type-filter-select'), function(_i, obj) {
            $(obj).hide();
          });

          if (select.hasClass('hide'))
            select.toggleClass('hide').show();
          else
            select.show();
        });
      }

      function closeModal() {
        $('#overlay').click();
      }

      function bindInputMasks() {
        $timeout(function() {
          $('.list-filter').children().last().find('.periodo').inputmask("99/99/9999 à 99/99/9999");
          $('.list-filter').children().last().find('.cpf').inputmask("999.999.999-99");
        });
      }

      function getUrlFilterParams() {
        var urlParams = '',
          filters = $('.list-filter').children();

        var len = filters.length;
        for (var i = 0; i < len; i++) {
          var $el = $(filters[i]);

          var filter = $el.find('.filter-select').val();
          var type = $el.children('.' + filter).val();

          if (!filter || !type) continue;

          if (i !== (len - 1))
            urlParams += filter + '=' + type + '&';
          else
            urlParams += filter + '=' + type;
        }

        return urlParams;
      }

      function hasFilter() {
        return (!$('.filter-select').val()) ? false : true;
      }

      function insertSummaryButton() {
        $timeout(function() {
          var $el = $('.ng-table-pager'),
              btn = $('<button ng-click="vm.showSummaryModal()"></button>').addClass('btn btn-sm btn-primary summary-btn').text('Resumo');

          $el.prepend($compile(btn)(scope));
        });
      }

      function showSummaryModal() {
        var $el = $('#production-summary-modal');

        $el.modal('show');
        Spin.start($('.production-summary-modal'), { relative: true });

        vm.getProductionSummary();
      }
    }
  }
})();
