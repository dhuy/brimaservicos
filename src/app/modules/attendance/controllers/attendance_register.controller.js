(function() {
  'use strict';

  angular
    .module('brimaservicos.attendance')
    .controller('AttendanceRegisterController', AttendanceRegisterController);

  /** @ngInject */
  function AttendanceRegisterController(AttendanceService, Toastr, Message, Spin, $rootScope, $state) {
    var vm = this;

    vm.breadcrumbs = [];
    vm.clientNotFound = false;

    vm.getClientByCpf = getClientByCpf;
    vm.registerAttendance = registerAttendance;

    _init();

    function _init() {
      vm.breadcrumbs.push({
        name: 'Cadastrar Atendimento',
        icon: 'fa fa-plus-circle',
        state: 'main.attendance_register',
        isActive: true
      });
    }

    function getClientByCpf() {
      var elToSpin = $rootScope.getElement('.box-attendance');
      Spin.start(elToSpin, { relative: true });

      AttendanceService.getClientByCpf(vm.searchedCpf)
        .then(function(response) {
          if (response.data.detail.cliente == null) {
            Toastr.warning(Message.get('cpf_not_registered'), null, {timeOut: 3000, progressBar: true});
            Spin.stop(elToSpin);
            vm.clientNotFound = true;
            vm.currentClient = null;
            return;
          }

          vm.currentClient = response.data.detail;

          Spin.stop(elToSpin);
        })
        .catch(function(response, status, headers, config) {
          Spin.stop(elToSpin);

          Toastr.error(Message.get('cant_list_client'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function registerAttendance() {
      if (!vm.validateFields()) {
        Toastr.info(Message.get('fill_all_fields'), null, {timeOut: 3000, progressBar: true});
        return;
      }

      var elToSpin = $rootScope.getElement('.box-attendance');
      Spin.start(elToSpin, { relative: true });

      AttendanceService.registerAttendance(vm.getAttendanceObject())
        .then(function(response) {
          Spin.stop(elToSpin);

          Toastr.success(Message.get('attendance_registered'), null, {timeOut: 3000, progressBar: true});

          $state.reload();
        })
        .catch(function(response, status, headers, config) {
          Spin.stop(elToSpin);

          if (response.data)
            Toastr.error(response.data.detail, null, {timeOut: 3000, progressBar: true});
          else
            Toastr.error(Message.get('cant_list_client'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }
  }
})();
