(function() {
  'use strict';

  angular
    .module('brimaservicos.attendance')
    .controller('AttendanceListController', AttendanceListController);

  /** @ngInject */
  function AttendanceListController(Spin, Toastr, Message, $filter, $rootScope, ngTableParams, AttendanceService) {
    var vm = this;

    var _currentParams;

    vm.breadcrumbs = [];
    vm.attendances = [];
    vm.editedInputs = [];

    vm.filterAttendance = filterAttendance;
    vm.editNewAttendance = editNewAttendance;

    _init();

    function _init() {
      var elToSpin = $rootScope.getElement('body');
      Spin.start(elToSpin, { relative: true });

      vm.breadcrumbs.push({
        name: 'Listar Atendimento',
        icon: 'fa fa-bars',
        state: 'main.attendance_list',
        isActive: true
      });

      _runNgTable();

      Spin.stop(elToSpin);
    }

    function filterAttendance(params, isEditing) {
      _currentParams = params;

      var elToSpin = $rootScope.getElement('body');
      Spin.start(elToSpin, { relative: true });

      AttendanceService.filterAttendance(params)
        .then(function(response) {
          Spin.stop(elToSpin);

          vm.attendances = response.data._embedded.atendimento;

          _updateNgTable();

          if (isEditing)
            Toastr.success(Message.get('attendance_edited'), null, {timeOut: 3000, progressBar: true});
        })
        .catch(function(response, status, headers, config) {
          Spin.stop(elToSpin);

          if (response.data)
            Toastr.error(response.data.detail, null, {timeOut: 3000, progressBar: true});
          else
            Toastr.error(Message.get('cant_list_attendance'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function editNewAttendance(id, obj, fn) {
      var elToSpin = $rootScope.getElement('body');
      Spin.start(elToSpin, { relative: true });

      AttendanceService.editAttendance(id, obj)
        .then(function(response) {
          Spin.stop(elToSpin);

          fn(_currentParams);
        })
        .catch(function(response, status, headers, config) {
          Spin.stop(elToSpin);

          if (response.data)
            Toastr.error(response.data.detail, null, {timeOut: 3000, progressBar: true});
          else
            Toastr.error(Message.get('cant_list_attendance'), null, {timeOut: 3000, progressBar: true});

          fn();
        })
      ;
    }

    /**
     * ngTable
     */
    function _updateNgTable() {
      vm.tableParams.reload();
    }

    function _runNgTable() {
      vm.tableParams = new ngTableParams({
        page: 1,
        count: 10
      }, {
        counts: [],
        total: vm.attendances.length,
        getData: function ($defer, params) {
          var processedData = vm.attendances;

          if (Object.keys(params.sorting()).length !== 0)
            processedData = $filter('orderBy')(processedData, params.orderBy());

          $defer.resolve(processedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          params.total(processedData.length);
          (!processedData.length) ? vm.noResult = true : vm.noResult = false;
        }
      });
    }
  }
})();
