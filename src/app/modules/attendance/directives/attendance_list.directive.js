(function() {
  'use strict';

  angular
    .module('brimaservicos.attendance')
    .directive('attendanceList', AttendanceList);

  /** @ngInject */
  function AttendanceList(Toastr, Message, $filter) {
    var directive = {
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attrs) {
      var vm = scope.vm,
          _mode = '';

      vm.disableEditBtns = false;

      vm.checkfilterAttendance = checkfilterAttendance;
      vm.editAttendance = editAttendance;
      vm.saveEditedAttendance = saveEditedAttendance;

      _init();

      function _init() {
        $(document).ready(function() {
          _registerInputMasks();
          _registerFilterChangeEvent();
          _setOnlyNumericInputs();
        });
      }

      function _fillEmptyInputs() {
        Toastr.info(Message.get('fill_empty_fields'), null, {timeOut: 3000, progressBar: true});
      }

      function checkfilterAttendance() {
        if (!vm.cpfFromFilter) {
          Toastr.info(Message.get('type_a_cpf'), null, {timeOut: 3000, progressBar: true});
          $('input[name="attendance-cpf"]').focus();
          return;
        }

        var selectValue = $('.filter-select').val();
        var obj = {
          cpf: vm.cpfFromFilter
        };

        if (selectValue === 'emprestimo') {
          if (!vm.opFromFilter) {
            _fillEmptyInputs();
            return;
          } else {
            obj['operacao'] = vm.opFromFilter;
          }
        }

        if (selectValue === 'conta') {
          if (!vm.agFromFilter || !vm.accFromFilter) {
            _fillEmptyInputs();
            return;
          } else {
            obj['agencia'] = vm.agFromFilter;
            obj['conta'] = vm.accFromFilter;
          }
        }

        vm.filterAttendance(obj);
      }

      function editAttendance(id) {
        var editBtn = $('#edit-btn-' + id),
            saveBtn = $('#save-btn-' + id);

        var lAgencia = $('#label-agencia-' + id), iAgencia = $('#input-agencia-' + id),
            lConta = $('#label-conta-' + id), iConta = $('#input-conta-' + id),
            lOperacao = $('#label-operacao-' + id), iOperacao = $('#input-operacao-' + id);

        vm.disableEditBtns = true;

        if (lAgencia.text() !== '-') {
          _toggle(lAgencia); _toggle(iAgencia);
          _toggle(lConta); _toggle(iConta);

          iAgencia.focus();

          iAgencia.val(lAgencia.text());
          iConta.val(lConta.text());

          _mode = 'conta';
        } else {
          _toggle(lOperacao); _toggle(iOperacao);

          iOperacao.focus();

          iOperacao.val(lOperacao.text());

          _mode = 'emprestimo';
        }

        _toggle(editBtn); _toggle(saveBtn);
      }

      function saveEditedAttendance(id) {
        var editBtn = $('#edit-btn-' + id),
            saveBtn = $('#save-btn-' + id),
            attendance;

        var lAgencia = $('#label-agencia-' + id), iAgencia = $('#input-agencia-' + id),
          lConta = $('#label-conta-' + id), iConta = $('#input-conta-' + id),
          lOperacao = $('#label-operacao-' + id), iOperacao = $('#input-operacao-' + id);

        vm.disableEditBtns = false;

        if (_mode == 'conta') {
          if (!iAgencia.val() || !iConta.val()) {
            Toastr.info(Message.get('fill_empty_fields'), null, {timeOut: 3000, progressBar: true});
            return;
          } else {
            attendance = angular.copy($filter('filter')(vm.attendances, function(el) { return el.id == id; }));
            attendance[0]['agencia'] = iAgencia.val();
            attendance[0]['conta'] = iConta.val();

            _toggle(lAgencia); _toggle(iAgencia);
            _toggle(lConta); _toggle(iConta);
          }
        } else if (_mode == 'emprestimo') {
          if (!iOperacao.val()) {
            Toastr.info(Message.get('fill_empty_fields'), null, {timeOut: 3000, progressBar: true});
            return;
          } else {
            attendance = angular.copy($filter('filter')(vm.attendances, function(el) { return el.id == id; }));
            attendance[0]['operacao'] = iOperacao.val();

            _toggle(lOperacao); _toggle(iOperacao);
          }
        }

        var fn = function(params) {
          _toggle(editBtn); _toggle(saveBtn);

          if (params)
            vm.filterAttendance(params, true);
        };

        vm.editNewAttendance(id, attendance[0], fn);
      }

      function _toggle(element) {
        element.toggleClass('hide');
      }

      function _registerFilterChangeEvent() {
        $(document).on('change', $('.filter-select'), function(e) {
          if (!$(e.target).hasClass('filter-select')) return;

          var $el = $(e.target),
            value = $el.val(),
            li = $el.parent(),
            select = li.find('.' + value);

          $.each(li.children('.type-filter-select'), function(_i, obj) {
            $(obj).hide();
          });

          if (select.hasClass('hide'))
            select.toggleClass('hide').show();
          else
            select.show();
        });
      }

      function _setOnlyNumericInputs() {
        $(document).on('keydown', $('input.numeric'), function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
          }
        });
      }

      function _registerInputMasks() {
        $('input[name="attendance-cpf"]').inputmask("999.999.999-99");
      }
    }
  }
})();
