(function() {
  'use strict';

  angular
    .module('brimaservicos.attendance')
    .directive('attendanceRegister', AttendanceRegister);

  /** @ngInject */
  function AttendanceRegister(CpfValidator, Toastr, Message) {
    var directive = {
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attrs) {
      var vm = scope.vm;

      vm.getClientToRegisterAttendance = getClientToRegisterAttendance;
      vm.noAnswerRefreshForm = noAnswerRefreshForm;
      vm.validateFields = validateFields;
      vm.getAttendanceObject = getAttendanceObject;

      _init();

      function _init() {
        $(document).ready(function() {
          _registerInputMasks();
          _registerFilterChangeEvent();
          _setOnlyNumericInputs();
        });
      }

      function _registerFilterChangeEvent() {
        $(document).on('change', $('.filter-select'), function(e) {
          if (!$(e.target).hasClass('filter-select')) return;

          var $el = $(e.target),
            value = $el.val(),
            li = $el.parent(),
            select = li.find('.' + value);

          $.each(li.children('.type-filter-select'), function(_i, obj) {
            $(obj).hide();
          });

          if (select.hasClass('hide'))
            select.toggleClass('hide').show();
          else
            select.show();
        });
      }

      function _setOnlyNumericInputs() {
        $(document).on('keydown', $('input.numeric'), function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
          }
        });
      }

      function getClientToRegisterAttendance() {
        if (!vm.searchedCpf) {
          Toastr.info(Message.get('type_a_cpf'), null, {timeOut: 3000, progressBar: true});
          $('.form-cpf-register-attendance').find('input[name="cpf-attendance"]').focus();
          return;
        }

        if (!CpfValidator.isValid(vm.searchedCpf)) {
          Toastr.warning(Message.get('invalid_cpf'), null, {timeOut: 3000, progressBar: true});
          return;
        }

        vm.getClientByCpf();
      }

      function validateFields() {
        var isValid = true,
            value = $('.filter-select').val();

        if (value == null) {
          isValid = false;
        }
        else {
          if (value == 'conta') {
            if (($('.opt-ag').val() == '') || $('.opt-acc').val() == '')
              isValid = false;
          } else if (value == 'emprestimo') {
            if ($('.opt-op').val() == '') {
              isValid = false;
            }
          }
        }

        return isValid;
      }

      function getAttendanceObject() {
        var obj = {},
            value = $('.filter-select').val();

        obj['cpf'] = vm.searchedCpf;

        if (value == 'emprestimo') {
          obj['operacao'] = $('.opt-op').val();
        } else if (value == 'conta') {
          obj['agencia'] = $('.opt-ag').val();
          obj['conta'] = $('.opt-acc').val();
        }

        return obj;
      }

      function noAnswerRefreshForm() {
        var $el = $('.form-cpf-register-attendance').find('input[name="cpf-attendance"]');

        vm.clientNotFound = false;
        $el.val('');
        $el.focus();
      }

      function _registerInputMasks() {
        $('.form-cpf-register-attendance').find('input[name="cpf-attendance"]').inputmask("999.999.999-99");
      }
    }
  }
})();
