(function() {
  'use strict';

  angular
    .module('brimaservicos.attendance')
    .factory('AttendanceService', AttendanceService);

  /** @ngInject */
  function AttendanceService($http, ENV, LoginService) {
    var factory = {
      getClientByCpf: getClientByCpf,
      registerAttendance: registerAttendance,
      filterAttendance: filterAttendance,
      editAttendance: editAttendance
    };
    return factory;

    function getClientByCpf(cpf) {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/clientes?cpf=' + cpf,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }

    function registerAttendance(body) {
      return $http({
        method: 'POST',
        url: ENV().ADDR + '/atendimento',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        },
        data: body
      })
    }

    function filterAttendance(params) {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/atendimento',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        },
        params: params
      })
    }

    function editAttendance(id, body) {
      return $http({
        method: 'PUT',
        url: ENV().ADDR + '/atendimento/' + id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        },
        data: body
      })
    }
  }
})();
