(function() {
  'use strict';

  angular
    .module('brimaservicos.dashboard')
    .controller('DashboardController', DashboardController);

  /** @ngInject */
  function DashboardController($state, DashboardService, Spin, Message, $rootScope, $filter, Toastr) {
    var vm = this,
        elToSpinGlobal,
        summaryLoaded = false,
        goalsLoaded = false,
        fulfillmentLoaded = false,
        workedDaysLoaded = false;

    vm.breadcrumbs = [];

    vm.go = go;
    vm.getSummary = getSummary;
    vm.getGoal = getGoal;
    vm.getFulfillment = getFulfillment;
    vm.getWorkedDays = getWorkedDays;

    _init();

    function _init() {
      vm.breadcrumbs.push({
        name: 'Dashboard',
        icon: 'fa fa-dashboard',
        state: 'main.dashboard',
        isActive: true
      });

      elToSpinGlobal = $rootScope.getElement('body');
      Spin.start(elToSpinGlobal, { relative: true });

      vm.getSummary();
      vm.getGoal();
      vm.getFulfillment();
      vm.getWorkedDays();
    }

    function getSummary() {
      DashboardService.getSummary()
        .then(function(response) {
          _setServiceObjects(response.data._embedded.dashboard_operador_resumo);

          summaryLoaded = true;
          _checkDataToStopSpin();
        })
        .catch(function(response, status, headers, config) {
          summaryLoaded = true;
          _checkDataToStopSpin();

          Toastr.error(Message.get('cant_get_summary'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function getGoal() {
      DashboardService.getGoal()
        .then(function(response) {
          vm.goals = _modifyGoalsObject(response.data._embedded.dashboard_operador_meta);

          goalsLoaded = true;
          _checkDataToStopSpin();
        })
        .catch(function(response, status, headers, config) {
          goalsLoaded = true;
          _checkDataToStopSpin();

          Toastr.error(Message.get('cant_get_summary'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function getFulfillment() {
      DashboardService.getFulfillment()
        .then(function(response) {
          vm.buildFulfillmentChart(response.data._embedded.dashboard_operador_desempenho);

          fulfillmentLoaded = true;
          _checkDataToStopSpin();
        })
        .catch(function(response, status, headers, config) {
          fulfillmentLoaded = true;
          _checkDataToStopSpin();

          Toastr.error(Message.get('cant_get_summary'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function getWorkedDays() {
      DashboardService.getWorkedDays()
        .then(function(response) {
          vm.workedDays = response.data._embedded.dashboard_operador_dias_trabalhados;

          vm.setProgressBarWidth(vm.workedDays.length);

          workedDaysLoaded = true;
          _checkDataToStopSpin();
        })
        .catch(function(response, status, headers, config) {
          workedDaysLoaded = true;
          _checkDataToStopSpin();

          Toastr.error(Message.get('cant_get_worked_days'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function _checkDataToStopSpin() {
      if (summaryLoaded && goalsLoaded && fulfillmentLoaded && workedDaysLoaded)
        Spin.stop(elToSpinGlobal);
    }

    function _modifyGoalsObject(goals) {
      var valorTotal = goals[0].valortotalservicos;
      var flagActiveProgress = false;

      goals.unshift({
        valor_minimo: '0.00',
        valor_maximo: '84999.99',
        valortotalservicos: valorTotal
      });

      var i = 0, len = goals.length;
      for (i; i < len; i++) {
        goals[i]['opacity'] = 'low';
        goals[i]['active'] = false;
        goals[i]['progressBarClass'] = 'success';
        goals[i]['width'] = '100';

        if (valorTotal >= goals[i].valor_maximo) {
          goals[i]['title'] = 'Parabéns, você alcançou essa faixa!';
          goals[i]['completed'] = '100%';
          goals[i]['type'] = $filter('RealBrasileiro')(valorTotal) + ' ✔ - Parabéns';
        } else {
          var leftover = Math.round((goals[i].valor_maximo - valorTotal) * 100) / 100;

          if (!flagActiveProgress) {
            goals[i]['opacity'] = 'high';
            goals[i]['title'] = 'Valor restante: ' + $filter('RealBrasileiro')(leftover);
            goals[i]['completed'] = Math.round(valorTotal * 100 / goals[i].valor_maximo * 100) / 100 + '%';
            goals[i]['type'] = $filter('RealBrasileiro')(valorTotal);
            goals[i]['active'] = true;
            goals[i]['progressBarClass'] = 'warning';
            goals[i]['width'] = Math.round(valorTotal * 100 / goals[i].valor_maximo * 100) / 100;

            flagActiveProgress = true;
          } else {
            goals[i]['title'] = $filter('RealBrasileiro')(leftover) + ' para alcançar essa faixa.';
            goals[i]['completed'] = '0%';
            goals[i]['type'] = '';
          }
        }
      }

      return goals;
     }

    function go(path) {
      $state.go(path);
    }

    function _setServiceObjects(services) {
      services.push({
        'servico': '109',
        'qtd': '16',
        'valorcomissao': '0',
        'valortotalservicos': '0',
        'valor_minimo': '0',
        'valor_maximo': '999.99',
        'comissao_operador_tabela': '21',
        'comissao_operador_faixa': '1',
        'data_movimentacao': '2017-01-19'
      });

      vm['emprestimo'] = $filter('filter')(services, function(el) { return el.servico === '68'; })[0];
      vm['ativacaoConta'] = $filter('filter')(services, function(el) { return el.servico === '109'; })[0];
    }
  }
})();
