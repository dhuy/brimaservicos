(function() {
  'use strict';

  angular
    .module('brimaservicos.dashboard')
    .factory('DashboardService', DashboardService);

  /** @ngInject */
  function DashboardService($http, ENV, LoginService) {
    var factory = {
      getSummary: getSummary,
      getGoal: getGoal,
      getFulfillment: getFulfillment,
      getWorkedDays: getWorkedDays
    };
    return factory;

    function getSummary() {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/dashboard_operador_resumo',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }

    function getGoal() {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/dashboard_operador_meta',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }

    function getFulfillment() {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/dashboard_operador_desempenho',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }

    function getWorkedDays() {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/dashboard_operador_dias_trabalhados',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }
  }
})();
