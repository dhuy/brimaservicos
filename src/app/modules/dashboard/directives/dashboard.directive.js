(function() {
  'use strict';

  angular
    .module('brimaservicos.dashboard')
    .directive('dashboard', Dashboard);

  /** @ngInject */
  function Dashboard($filter) {
    var directive = {
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attrs) {
      var vm = scope.vm;

      vm.buildFulfillmentChart = buildFulfillmentChart;
      vm.setProgressBarWidth = setProgressBarWidth;

      function setProgressBarWidth(length) {
        $('.custom-progressbar').find('.progress').width((115 * length) + 'px');
      }

      function buildFulfillmentChart(chartObj) {
        var accounts = [],
            loans = [],
            data = [],
            comissionMonth = [];

        var i = 0, len = chartObj.length;
        for (i; i < len; i++) {
          accounts.push(chartObj[i].qtd_contas);
          loans.push($filter('RealBrasileiro')(chartObj[i].valortotalservicos));
          comissionMonth.push(chartObj[i].mes_pgto);

          data.push({
            'y': $filter('uppercase')(chartObj[i].mes_extenso), 'x': $filter('FixComission')(chartObj[i].valorcomissao)
          });
        }

        Morris.Bar({
          element: 'bar-chart',
          resize: true,
          data: data,
          xkey: ['y'],
          ykeys: ['x'],
          labels: ['_'],
          hideHover: 'auto',
          stacked: true,
          barColors: ['#00a65a'],
          gridIntegers: true,
          gridTextColor:['#888'],
          gridTextSize: 10,
          axes: true,
          grid: true,
          yLabelFormat: function (y) {
            var parts = y.toString().split('.');

            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');

            if (!parts[1]) {
              parts[0] = parts[0] + ',00';
            } else {
              if (parts[1].length == 1) {
                parts[1] = parts[1] + '0';
              }
            }

            return parts.join(',');
          },
          hoverCallback: function (index, options, content, row) {
            return row['y'] +
              '<br>Empréstimos: ' + loans[index] +
              '<br>Contas: ' + accounts[index] +
              '<br>Comissão: ' + $filter('RealBrasileiro')(data[index].x) +
              '<br> Mês Pgto Comissão: ' + comissionMonth[index];
          }
        });
      }
    }
  }
})();
