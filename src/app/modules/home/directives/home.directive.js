(function() {
  'use strict';

  angular
    .module('brimaservicos.home')
    .directive('home', home);

  /** @ngInject */
  function home() {
    var directive = {
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attrs) {

    }
  }
})();
