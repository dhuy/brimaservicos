(function() {
  'use strict';

  angular
    .module('brimaservicos.home')
    .factory('HomeService', HomeService);

  /** @ngInject */
  function HomeService($http, ENV) {
    var factory = {
      blank: blank
    };
    return factory;

    function blank() {
      return true;
    }
  }
})();
