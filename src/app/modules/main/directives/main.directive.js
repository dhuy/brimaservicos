(function() {
  'use strict';

  angular
    .module('brimaservicos.main')
    .directive('main', Main);

  /** @ngInject */
  function Main() {
    var directive = {
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attrs) {

    }
  }
})();
