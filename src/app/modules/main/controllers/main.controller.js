(function() {
  'use strict';

  angular
    .module('brimaservicos.main')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(LoginService, $state, $scope, $rootScope) {
    var vm = this;

    vm.logout = logout;
    vm.go = go;

    _init();

    function _init() {
      $scope.$state = $state;
      $rootScope.setAdminLTEOptions();
    }

    function logout() {
      LoginService.logout();
      $rootScope.user = null;

      go('login');
    }

    function go(path) {
      $state.go(path);
    }
  }
})();
