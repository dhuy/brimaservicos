(function() {
  'use strict';

  angular
    .module('brimaservicos.login')
    .directive('login', login);

  /** @ngInject */
  function login(Spin) {
    var directive = {
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attrs) {
      var vm = scope.vm;

      vm.submitLoginForm = function() {
        Spin.start($('.login-form-wrapper'), { relative: true });

        if (!$('#lg_username').hasClass('form-invalid') &&
            !$('#lg_password').hasClass('form-invalid')) {

          var fn = function() {
            Spin.stop($('.login-form-wrapper'));
          };

          vm.auth(fn);
        }
      };

      // Options for Message
      //----------------------------------------------
      var options = {
        'btn-loading': '<i class="fa fa-spinner fa-pulse"></i>',
        'btn-success': '<i class="fa fa-check"></i>',
        'btn-error': '<i class="fa fa-remove"></i>',
        'msg-success': 'All Good! Redirecting...',
        'msg-error': 'Wrong login credentials!',
        'useAJAX': true
      };

      // Login Form
      //----------------------------------------------
      // Validation
      $("#login-form").validate({
        rules: {
          lg_username: "required",
          lg_password: "required"
        },
        errorClass: "form-invalid",
        messages: {
          lg_username: "Digite o usuário.",
          lg_password: "Digite a senha."
        }
      });

      // Register Form
      //----------------------------------------------
      // Validation
      $("#register-form").validate({
        rules: {
          reg_username: "required",
          reg_password: {
            required: true,
            minlength: 5
          },
          reg_password_confirm: {
            required: true,
            minlength: 5,
            equalTo: "#register-form [name=reg_password]"
          },
          reg_email: {
            required: true,
            email: true
          },
          reg_agree: "required"
        },
        errorClass: "form-invalid",
        errorPlacement: function( label, element ) {
          if( element.attr( "type" ) === "checkbox" || element.attr( "type" ) === "radio" ) {
            element.parent().append( label ); // this would append the label after all your checkboxes/labels (so the error-label will be the last element in <div class="controls"> )
          }
          else {
            label.insertAfter( element ); // standard behaviour
          }
        }
      });

      // Forgot Password Form
      //----------------------------------------------
      // Validation
      $("#forgot-password-form").validate({
        rules: {
          fp_email: "required"
        },
        errorClass: "form-invalid"
      });
    }
  }
})();
