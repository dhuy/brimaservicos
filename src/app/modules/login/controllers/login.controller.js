(function() {
  'use strict';

  angular
    .module('brimaservicos.login')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController(LoginService, Toastr, $state, $rootScope, Spin, Message) {
    var vm = this;

    vm.auth = auth;

    init();

    function init() {

    }

    function auth(callback) {
      var body = {
        grant_type: "password",
        username: vm.username,
        password: vm.password,
        client_id: "testclient",
        client_secret: "testsecret"
      };

      LoginService.auth(body)
        .then(function(response) {
          LoginService.storeToken(response.data);

          $rootScope.isLogin = true;

          callback();

          $state.go('main');
        })
        .catch(function(response, status, headers, config) {
          Spin.stop($rootScope.getElement('.login-form-wrapper'));

          if (response.data)
            Toastr.error(response.data.detail, null, {timeOut: 3000, progressBar: true});
          else
            Toastr.error(Message.get('cant_login'), Message.get('cant_login_description'), {timeOut: 3000, progressBar: true});
        })
      ;
    }
  }
})();
