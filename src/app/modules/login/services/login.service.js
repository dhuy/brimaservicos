(function() {
  'use strict';

  angular
    .module('brimaservicos.login')
    .factory('LoginService', LoginService);

  /** @ngInject */
  function LoginService($http, ENV, $cookies) {
    var factory = {
      auth: auth,
      logout: logout,
      getToken: getToken,
      storeToken: storeToken,
      getUserInfo: getUserInfo
    };
    return factory;

    function getToken() {
      return $cookies.get('Token');
    }

    function auth(body) {
      return $http({
        method: 'POST',
        url: ENV().ADDR + '/oauth',
        headers: {
          'Content-Type': 'application/json'
        },
        data: body
      })
    }

    function getUserInfo() {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/user_information',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }
      })
    }

    function logout() {
      $cookies.remove('Token');
    }

    function storeToken(obj) {
      $cookies.put('Token', obj.token_type + ' ' + obj.access_token);
    }
  }
})();
