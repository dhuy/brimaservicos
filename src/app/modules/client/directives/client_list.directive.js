(function() {
  'use strict';

  angular
    .module('brimaservicos.client')
    .directive('clientList', ClientList);

  /** @ngInject */
  function ClientList() {
    var directive = {
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attrs) {
      var vm = scope.vm;

      _init();

      function _init() {
        $(document).ready(function() {
          _registerInputMasks();
        });
      }

      function _registerInputMasks() {
        $('.form-search-client-cpf').find('input[name="search-client-cpf"]').inputmask("999.999.999-99");
      }
    }
  }
})();
