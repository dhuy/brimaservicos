(function() {
  'use strict';

  angular
    .module('brimaservicos.client')
    .directive('clientRegister', ClientRegister);

  /** @ngInject */
  function ClientRegister(Toastr, Message, CpfValidator, $timeout) {
    var directive = {
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attrs) {
      var vm = scope.vm,
          registerForm;

      vm.clientContacts = [];

      vm.addContact = addContact;
      vm.removeContact = removeContact;
      vm.clearClientForm = clearClientForm;
      vm.setFieldsInEditMode = setFieldsInEditMode;

      _init();

      function _init() {
        $(document).ready(function() {
          registerForm = $('.client-form');

          _registerInputMasks();
          _registerValidationObject();
          _onChangeContactSelect();
        });
      }

      function addContact() {
        var label = $('.select-contact-type').find('option:selected').text(),
          type = $('.select-contact-type').val(),
          inputValue = $('.input-group-container').children('.input-group').not('.hide').find('input').val();

        vm.clientContacts.push({
          label: label,
          tipo: type,
          contato: inputValue
        });
      }

      function removeContact(i) {
        vm.clientContacts.splice(i, 1);
      }

      function clearClientForm(editMode) {
        var registerForm = $('.client-form'),
          select = $('.select-contact-type'),
          defaultInput = $('.default');

        registerForm.find('input[name="client-form-name"]').val('');
        registerForm.find('input[name="client-form-birth"]').val('');

        if (vm.isEdit && editMode) {
          registerForm.find('input[name="client-form-cpf"]').attr('disabled', false);
          registerForm.find('input[name="client-form-cpf"]').val('');
          vm.isEdit = false;
        }

        registerForm.find('input[name="client-form-contact"]').val('');

        if (select.val() != 'default')
          select.val('default');

        _clearInputs();

        defaultInput.removeClass('hide');

        vm.clientContacts = [];

        $timeout(function() {
          $('label.error').remove();
        });
      }

      function setFieldsInEditMode(client) {
        registerForm.find('input[name="client-form-name"]').val(client.cliente.nome);
        registerForm.find('input[name="client-form-birth"]').val(client.cliente.data_nascimento);

        registerForm.find('input[name="client-form-cpf"]').val(client.cliente.cpf);
        registerForm.find('input[name="client-form-cpf"]').attr('disabled', true);

        vm.clientContacts = client.contato;
      }

      function _registerInputMasks() {
        registerForm.find('input[name="client-form-birth"]').inputmask("99/99/9999");
        registerForm.find('input[name="client-form-cpf"]').inputmask("999.999.999-99");
        registerForm.find('input[name="celular-form-contact"]').inputmask("(99) 99999-9999");
        registerForm.find('input[name="residencial-form-contact"]').inputmask("(99) 9999-9999");
        registerForm.find('input[name="comercial-form-contact"]').inputmask("(99) 9999-9999");
        registerForm.find('input[name="recado-form-contact"]').inputmask("(99) 9999-9999");
      }

      function _registerValidationObject() {
        registerForm.validate({
          rules: {
            'client-form-email': { email: true }
          },
          messages: {
            'client-form-name': { required: 'Campo obrigatório.' },
            'client-form-cpf': { required: 'Campo obrigatório.' },
            'client-form-email': { email: 'Email inválido.', required: 'Campo obrigatório.' }
          },
          submitHandler: function(form) {
            if (!CpfValidator.isValid($(form).find('input[name="client-form-cpf"]').val())) {
              Toastr.warning(Message.get('invalid_cpf'), null, {timeOut: 3000, progressBar: true});
              return;
            }

            if (vm.clientContacts.length < 1) {
              Toastr.info(Message.get('no_phones_found'), null, {timeOut: 3000, progressBar: true});
              $(form).find('input[name="client-form-contact"]').focus();
              return;
            }

            if (vm.isEdit)
              _editClient($(form));
            else
              _registerClient($(form));
          }
        });
      }

      function _onChangeContactSelect() {
        $('.select-contact-type').change(function(e) {
          var $el = $(e.target),
            value = $el.val(),
            container = $('.input-group-container'),
            defaultInput = $('.default'),
            inputGroup = container.find('.' + value + '-input-group');

          _clearInputs();

          if (value === 'default') {
            defaultInput.removeClass('hide');
            return;
          }

          if (!defaultInput.hasClass('hide'))
            defaultInput.addClass('hide');

          if (inputGroup.hasClass('hide'))
            inputGroup.removeClass('hide');
          else
            inputGroup.addClass('hide');
        });
      }

      function _registerClient($form) {
        vm.registerClient({
          nome: $form.find('input[name="client-form-name"]').val(),
          data_nascimento: $form.find('input[name="client-form-birth"]').val(),
          cpf: $form.find('input[name="client-form-cpf"]').val(),
          contato: vm.clientContacts
        });
      }

      function _editClient($form) {
        vm.editClient({
          nome: $form.find('input[name="client-form-name"]').val(),
          data_nascimento: $form.find('input[name="client-form-birth"]').val(),
          cpf: $form.find('input[name="client-form-cpf"]').val(),
          contato: vm.clientContacts
        });
      }

      function _clearInputs() {
        $.each($('.input-group-container').children().not('.default'), function(i, obj) {
          if (!$(obj).hasClass('hide'))
            $(obj).addClass('hide');

          $(obj).find('input').val('');
        });
      }
    }
  }
})();
