(function() {
  'use strict';

  angular
    .module('brimaservicos.client')
    .controller('ClientListController', ClientListController);

  /** @ngInject */
  function ClientListController(Toastr, Spin, Message, $rootScope, ClientService, CpfValidator, $state) {
    var vm = this;

    vm.breadcrumbs = [];
    vm.getClientByCpf = getClientByCpf;
    vm.editClient = editClient;

    _init();

    function _init() {
      vm.breadcrumbs.push({
        name: 'Listar Cliente',
        icon: 'fa fa-bars',
        state: 'main.client_list',
        isActive: true
      });
    }

    function getClientByCpf() {
      if (!vm.searchedCpf) {
        Toastr.info(Message.get('type_a_cpf'), null, {timeOut: 3000, progressBar: true});
        return;
      }

      if (!CpfValidator.isValid(vm.searchedCpf)) {
        Toastr.warning(Message.get('invalid_cpf'), null, {timeOut: 3000, progressBar: true});
        return;
      }

      var elToSpin = $rootScope.getElement('.box-client-list');
      Spin.start(elToSpin, { relative: true });

      ClientService.getClientByCpf(vm.searchedCpf)
        .then(function(response) {
          if (response.data.detail.cliente == null) {
            Toastr.warning(Message.get('cpf_not_registered'), null, {timeOut: 3000, progressBar: true});
            Spin.stop(elToSpin);
            return;
          }

          vm.currentClient = _prepareClientObj(response.data.detail);

          Spin.stop(elToSpin);
        })
        .catch(function(response, status, headers, config) {
          Spin.stop(elToSpin);

          Toastr.error(Message.get('cant_list_client'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function editClient() {
      $state.go('main.client_register', { client: vm.currentClient });
    }

    function _prepareClientObj(obj) {
      var newContactArray = [];

      var i = 0, len = obj['contato'].length;
      for (i; i < len; i++) {
        switch (obj['contato'][i].tipo) {
          case '1':
            newContactArray.push({
              id: obj['contato'][i].id,
              type: 'Email',
              contato: obj['contato'][i].contato
            });
            break;

          case '2':
            newContactArray.push({
              id: obj['contato'][i].id,
              type: 'Celular',
              contato: obj['contato'][i].contato
            });
            break;

          case '3':
            newContactArray.push({
              id: obj['contato'][i].id,
              type: 'Residencial',
              contato: obj['contato'][i].contato
            });
            break;

          case '4':
            newContactArray.push({
              id: obj['contato'][i].id,
              type: 'Comercial',
              contato: obj['contato'][i].contato
            });
            break;

          case '5':
            newContactArray.push({
              id: obj['contato'][i].id,
              type: 'Recado',
              contato: obj['contato'][i].contato
            });
            break;
        }
      }

      obj['contato'] = newContactArray;

      return obj;
    }
  }
})();
