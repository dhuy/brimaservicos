(function() {
  'use strict';

  angular
    .module('brimaservicos.client')
    .controller('ClientRegisterController', ClientRegisterController);

  /** @ngInject */
  function ClientRegisterController(ClientService, Message, Toastr, Spin, $rootScope, $stateParams, $timeout) {
    var vm = this;

    vm.registerClient = registerClient;
    vm.editClient = editClient;

    vm.breadcrumbs = [];
    vm.isEdit = false;

    _init();

    function _init() {
      vm.breadcrumbs.push({
        name: 'Cadastrar Cliente',
        icon: 'fa fa-plus-circle',
        state: 'main.client_register',
        isActive: true
      });

      if ($stateParams.client) {
        vm.isEdit = true;

        $timeout(function() {
          vm.setFieldsInEditMode($stateParams.client);
        });
      }
    }

    function _prepareClientObject(obj) {
      var newObject = { nome: obj.nome, cpf: obj.cpf.replace(/[^\d]+/g,''), contato: [] };

      if (obj.data_nascimento != '')
        newObject['data_nascimento'] = obj.data_nascimento;

      var i = 0, len = obj.contato.length;
      for (i; i < len; i++) {
        var tmp;

        if (vm.isEdit && obj.contato[i].id)
          tmp = { id: obj.contato[i].id, tipo: obj.contato[i].tipo, contato: obj.contato[i].contato };
        else
          tmp = { tipo: obj.contato[i].tipo, contato: obj.contato[i].contato };

        if (obj.contato[i].tipo != '1')
          tmp.contato = tmp.contato.replace(/[() -]/g, '');

        newObject.contato.push(tmp);
      }

      return newObject;
    }

    function registerClient(object) {
      var elToSpin = $rootScope.getElement('.register-client-box');
      Spin.start(elToSpin, { relative: true });

      ClientService.registerClient(_prepareClientObject(object))
        .then(function() {
          Spin.stop(elToSpin);

          vm.clearClientForm();

          Toastr.success(Message.get('client_registered'), null, {timeOut: 3000, progressBar: true});
        })
        .catch(function(response, status, headers, config) {
          Spin.stop(elToSpin);

          if (response.data.status == 422)
            Toastr.warning(response.data.detail, null, {timeOut: 3000, progressBar: true});
          else
            Toastr.error(Message.get('cant_register_client'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }

    function editClient(object) {
      var elToSpin = $rootScope.getElement('.register-client-box');
      Spin.start(elToSpin, { relative: true });

      ClientService.editClient(_prepareClientObject(object))
        .then(function() {
          Spin.stop(elToSpin);

          vm.clearClientForm(true);

          Toastr.success(Message.get('client_edited'), null, {timeOut: 3000, progressBar: true});
        })
        .catch(function(response, status, headers, config) {
          Spin.stop(elToSpin);

          if (response.data.status == 422)
            Toastr.warning(response.data.detail, null, {timeOut: 3000, progressBar: true});
          else
            Toastr.error(Message.get('cant_edit_client'), null, {timeOut: 3000, progressBar: true});
        })
      ;
    }
  }
})();
