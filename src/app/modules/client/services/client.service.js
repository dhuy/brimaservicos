(function() {
  'use strict';

  angular
    .module('brimaservicos.client')
    .factory('ClientService', ClientService);

  /** @ngInject */
  function ClientService($http, ENV, LoginService) {
    var factory = {
      registerClient: registerClient,
      editClient: editClient,
      getClientByCpf: getClientByCpf
    };
    return factory;

    function registerClient(body) {
      return $http({
        method: 'POST',
        url: ENV().ADDR + '/clientes',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        },
        data: body
      })
    }

    function editClient(body) {
      return $http({
        method: 'PUT',
        url: ENV().ADDR + '/clientes/' + body.cpf,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        },
        data: body
      })
    }

    function getClientByCpf(cpf) {
      return $http({
        method: 'GET',
        url: ENV().ADDR + '/clientes?cpf=' + cpf,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': LoginService.getToken()
        }
      })
    }
  }
})();
