(function() {
  'use strict';

  angular
    .module('brimaservicos')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        permission: 'public',
        templateUrl: 'app/modules/home/index.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      })
      .state('login', {
        url: '/login',
        permission: 'public',
        templateUrl: 'app/modules/login/index.html',
        controller: 'LoginController',
        controllerAs: 'vm'
      })
      .state('main', {
        url: '/main',
        permission: 'private',
        templateUrl: 'app/modules/main/index.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })
      .state('main.dashboard', {
        url: '/dashboard',
        permission: 'private',
        templateUrl: 'app/modules/dashboard/index.html',
        controller: 'DashboardController',
        controllerAs: 'vm'
      })
      .state('main.client_register', {
        url: '/client_register',
        permission: 'private',
        templateUrl: 'app/modules/client/views/client_register.html',
        controller: 'ClientRegisterController',
        controllerAs: 'vm',
        params: {
          client: null
        }
      })
      .state('main.client_list', {
        url: '/client_list',
        permission: 'private',
        templateUrl: 'app/modules/client/views/client_list.html',
        controller: 'ClientListController',
        controllerAs: 'vm'
      })
      .state('main.attendance_register', {
        url: '/attendance_register',
        permission: 'private',
        templateUrl: 'app/modules/attendance/views/attendance_register.html',
        controller: 'AttendanceRegisterController',
        controllerAs: 'vm',
        params: {
          client: null
        }
      })
      .state('main.attendance_list', {
        url: '/attendance_list',
        permission: 'private',
        templateUrl: 'app/modules/attendance/views/attendance_list.html',
        controller: 'AttendanceListController',
        controllerAs: 'vm'
      })
      .state('main.production', {
        url: '/production',
        permission: 'private',
        templateUrl: 'app/modules/production/index.html',
        controller: 'ProductionController',
        controllerAs: 'vm'
      })
    ;

    $urlRouterProvider.otherwise('/');
  }

})();
