(function() {
  'use strict';

  angular
    .module('brimaservicos')
    .constant('ENV', env);

  function env() {
    return {
      ADDR: 'http://186.228.153.107:8888'
    };

    // return {
    //   ADDR: 'http://54.187.106.98/api'
    // }
  }
})();
