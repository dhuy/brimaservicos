(function () {
  'use strict';

  angular
    .module('brimaservicos.helper')
    .factory('Message', Message);

  function Message() {
    var factory = {
      get: get
    };
    return factory;

    function get(key) {
      switch (key) {
        case 'server_error':
          return 'Ocorreu um erro no servidor.';
          break;

        case 'user_not_found':
          return 'Usuário ou Senha incorretos.';
          break;

        case 'cant_logout':
          return 'Não foi possível sair do sistema.';
          break;

        case 'cant_register_client':
          return 'Não foi possível cadastrar o cliente.';
          break;

        case 'cant_edit_client':
          return 'Não foi possível editar o cliente.';
          break;

        case 'cant_list_client':
          return 'Não foi possível buscar o cliente.';
          break;

        case 'cant_list_attendance':
          return 'Não foi possível listar os atendimentos.';
          break;

        case 'attendance_edited':
          return 'Atendimento editado com sucesso.';
          break;

        case 'attendance_registered':
          return 'Atendimento cadastrado com sucesso.';
          break;

        case 'fill_all_fields':
          return 'Por favor, preencha todos os campos.';
          break;

        case 'cpf_not_registered':
          return 'CPF não cadastrado.';
          break;

        case 'client_registered':
          return 'Cliente cadastrado com sucesso.';
          break;

        case 'client_edited':
          return 'Cliente atualizado com sucesso.';
          break;

        case 'cant_login':
          return 'Não foi possível realizar o login.';
          break;

        case 'cant_login_description':
          return 'Por favor, tente novamente mais tarde.';
          break;

        case 'session_expired':
          return 'Sessão expirada.';
          break;

        case 'invalid_cpf':
          return 'CPF inválido.';
          break;

        case 'type_a_cpf':
          return 'Digite um CPF.';
          break;

        case 'fill_empty_fields':
          return 'Preencha os campos vazios.';
          break;

        case 'no_phones_found':
          return 'É necessário ao menos um contato.';
          break;

        case 'cant_get_summary':
          return 'Não foi possível obter o resumo do usuário.';
          break;

        case 'cant_get_worked_days':
          return 'Não foi possível obter os dias trabalhados.';
          break;

        case 'cant_get_filters':
          return 'Não foi possível obter os filtros.';
          break;

        case 'no_result':
          return 'Nenhum resultado foi encontrado.';
          break;

        case 'choose_at_least_one_filter':
          return 'Escolha ao menos um filtro.';
          break;

        case 'choose_at_least_one_filter_type':
          return 'Escolha ao menos um tipo de filtro.';
          break;
      }
    }
  }
})();
