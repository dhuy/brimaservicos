(function() {
  'use strict';

  angular
    .module('brimaservicos.helper')
    .factory('CpfValidator', CpfValidator);

  function CpfValidator() {
    var factory = {
      isValid: isValid
    };
    return factory;

    function isValid(cpf) {
      var sum, rest;

      sum = 0;
      cpf = cpf.replace(/\./g, '').replace(/\-/g, '');

      if (cpf == "00000000000") return false;

      var i = 1;
      for (i; i<=9; i++) sum = sum + parseInt(cpf.substring(i-1, i)) * (11 - i);
      rest = (sum * 10) % 11;

      if ((rest == 10) || (rest == 11))
        rest = 0;

      if (rest != parseInt(cpf.substring(9, 10))) return false;

      sum = 0;

      var i = 1;
      for (i; i <= 10; i++) sum = sum + parseInt(cpf.substring(i-1, i)) * (12 - i);
      rest = (sum * 10) % 11;

      if ((rest == 10) || (rest == 11)) rest = 0;

      if (rest != parseInt(cpf.substring(10, 11) ))
        return false;

      return true;
    }
  }
})();
