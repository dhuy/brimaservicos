(function() {
  'use strict';

  angular
    .module('brimaservicos', [
      'ngCookies',
      'ngAnimate',
      'ngTable',

      'ui.router',
      'toastr',

      'brimaservicos.helper',
      'brimaservicos.filters',

      'brimaservicos.home',
      'brimaservicos.login',
      'brimaservicos.main',
      'brimaservicos.dashboard',
      'brimaservicos.client',
      'brimaservicos.production',
      'brimaservicos.attendance'
    ])
  ;
})();
